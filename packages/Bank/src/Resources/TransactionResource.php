<?php
namespace Api\Package\Bank\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionResource extends Resource
{
    public function __construct($resource)
    {
        self::withoutWrapping();
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'transactions',
            'attributes' => [
                'transactionId' => $this->resource->transactionId,
                'amount' => $this->resource->amount,
                'date' => $this->resource->created_at->format('d.m.Y')
            ],
        ];
    }
}
