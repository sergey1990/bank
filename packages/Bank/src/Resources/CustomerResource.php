<?php
namespace Api\Package\Bank\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CustomerResource extends Resource
{
    public function __construct($resource)
    {
        self::withoutWrapping();
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'customers',
            'id' => (string)$this->resource->id,
            'attributes' => [
                'name' => $this->resource->name,
                'cnp' => $this->resource->cnp,
            ],
        ];
    }
}
