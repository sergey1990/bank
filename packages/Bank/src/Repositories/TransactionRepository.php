<?php

namespace Api\Package\Bank\Repositories;

use Api\Package\Bank\Models\TransactionSum;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;

class TransactionRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "Api\\Package\\Bank\\Models\\Transaction";
    }

    /**
     * @param $customer
     * @return $this
     */
    public function byCustomer($customer)
    {
        $this->model->where('customerId', $customer->id);

        return $this;
    }

    /**
     * @param $data
     * @param $customer
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function createAndApplyToCustomer($data, $customer)
    {
        $this->makeModel();
        $this->model->amount = $data['amount'];

        return $customer
            ->transactions()
            ->save($this->model);
    }

    /**
     * @param null $limit
     * @param int $offset
     * @return mixed
     */
    public function paginateWithOffset($limit, $offset = 0)
    {
        $this->applyCriteria();
        $this->applyScope();

        $limit = $limit ?? config('bank.items_per_page');
        $offset = $offset ?? 0;

        $results = $this->model->offset($offset)->limit($limit)->get();

        $this->resetModel();
        $this->resetScope();

        $paginate = new LengthAwarePaginator($results,
            $this->model->count(),
            $limit,
            ceil($offset / $limit),
            ['path' => route('transaction.get')]);

        return $this->parserResult($paginate);
    }

    /**
     * @param Carbon $date
     * @return mixed
     */
    public function findByDate(Carbon $date)
    {
        $results = $this->model
            ->whereDate('created_at', $date->format('Y-m-d'))
            ->get();

        $this->resetModel();
        $this->resetScope();

        return $this->parserResult($results);
    }

    /**
     * @param $date
     * @param $sum
     */
    public function storeSumPerDate($date, $sum)
    {
        $metric = TransactionSum::firstOrNew(['date' => $date->format('Y-m-d 00:00:00')]);
        $metric->amount = $sum;
        $metric->date = $date->format('Y-m-d 00:00:00');
        $metric->save();
    }
}