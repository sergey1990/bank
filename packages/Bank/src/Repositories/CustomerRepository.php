<?php

namespace Api\Package\Bank\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class CustomerRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "Api\\Package\\Bank\\Models\\Customer";
    }
}