<?php

namespace Api\Package\Bank\Criterias;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class TransactionForSpecificCustomerCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('amount')) {
            $model = $model->where('amount', '=', $this->request->get('amount'));
        }

        if ($this->request->has('date')) {
            $model = $model->whereDate('created_at', $this->request->get('date'));
        }

        return $model;
    }
}