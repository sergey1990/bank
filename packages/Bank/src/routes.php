<?php

Route::group([
    'namespace' => 'Api\Package\Bank\Controllers',
    'middleware' => ['jwt.auth', 'api'],
    'prefix' => 'api',
], function () {

    /**
     * Customer routes.
     */
    Route::group([
        'prefix' => 'customer',
        'as' => 'customers.',
    ], function () {
        Route::post('/', ['as' => 'store', 'uses' => 'CustomerController@store']);

        /**
         * Transaction routes.
         */
        Route::group([
            'prefix' => '{customer}/transaction',
            'as' => 'transaction.',
        ], function () {
            Route::post('/', ['as' => 'store', 'uses' => 'TransactionController@store']);
            Route::get('/', ['as' => 'get', 'uses' => 'TransactionController@get']);
            Route::get('/{transaction}', ['as' => 'getOne', 'uses' => 'TransactionController@getOne']);

        });
    });

    /**
     * Transaction routes.
     */
    Route::group([
        'prefix' => 'transaction',
        'as' => 'transaction.',
    ], function () {
        Route::put('/{transaction}', ['as' => 'update', 'uses' => 'TransactionController@update']);
        Route::delete('/{transaction}', ['as' => 'delete', 'uses' => 'TransactionController@delete']);
        Route::get('/', ['as' => 'get', 'uses' => 'TransactionController@getAll']);

    });

});

