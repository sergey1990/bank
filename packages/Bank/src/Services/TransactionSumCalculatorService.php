<?php
namespace Api\Package\Bank\Services;

use Illuminate\Support\Collection;

class TransactionSumCalculatorService
{
    /**
     * @param Collection $transactions
     * @return mixed
     */
    public function calculate(Collection $transactions)
    {
        return $transactions->sum('amount');
    }
}
