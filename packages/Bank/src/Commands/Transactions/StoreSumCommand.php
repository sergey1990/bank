<?php

namespace  Api\Package\Bank\Commands\Transactions;

use Api\Package\Bank\Repositories\TransactionRepository;
use Api\Package\Bank\Services\TransactionSumCalculatorService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StoreSumCommand extends Command
{

    protected $transactionRepository;

    protected $calculateService;

    /**
     * StoreSumCommand constructor.
     * @param TransactionRepository $repository
     */
    public function __construct(TransactionRepository $repository, TransactionSumCalculatorService $service)
    {
        $this->transactionRepository = $repository;
        $this->calculateService = $service;

        parent::__construct();
    }
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactions:calculate-sum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Script stores the sum of all transactions from previous day';

    public function handle()
    {
        $previousDate = Carbon::now()->subDay();

        $sum = $this->calculateService->calculate(
            $this->transactionRepository->findByDate($previousDate)
        );

        $this->transactionRepository->storeSumPerDate($previousDate, $sum);
    }

}