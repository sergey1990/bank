<?php

namespace Api\Package\Bank\Providers;

use Api\Package\Bank\Commands\Transactions\StoreSumCommand;
use Illuminate\Support\ServiceProvider;
use Api\Package\Bank\Criterias\TransactionCriteria;
use Api\Package\Bank\Criterias\TransactionForSpecificCustomerCriteria;

class BankServiceProvider extends ServiceProvider
{

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->setupProviders();
        $this->prepareResources();
        $this->registerCommands();
        $this->registerServiceFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {

    }

    /**
     *
     */
    protected function setupProviders()
    {
        $this->app->register(AuthServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Prepare the package resources.
     *
     * @return void
     */
    protected function prepareResources()
    {
        // Use package routes.
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');

        $this->loadMigrationsFrom(__DIR__ . '/../path/to/migrations');

        // Publish config.
        $config = realpath(__DIR__ . '/../../config/config.php');
        $this->mergeConfigFrom($config, 'bank');
        $this->publishes([
            $config => config_path('bank.php'),
        ], 'config');

        // Publish migrations.
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../../database/migrations'));

    }

    /**
     *
     */
    protected function registerCommands()
    {
        $this->commands([
            StoreSumCommand::class,
        ]);
    }

    protected function registerServiceFactory()
    {
        $this->app->bind(TransactionCriteria::class, function ($app) {
            return new TransactionCriteria($app['request']);
        });

        $this->app->bind(TransactionForSpecificCustomerCriteria::class, function ($app) {
            return new TransactionForSpecificCustomerCriteria($app['request']);
        });
    }
}
