<?php

namespace Api\Package\Bank\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Api\Package\Bank\Models\Transaction;
use Api\Package\Bank\Models\Customer;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //Transaction::class => TransactionPolicy::class
    ];

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerGates();
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {

    }

    public function registerGates()
    {
        Gate::define('access-to-transaction', function (User $user, Customer $customer, Transaction $transaction) {
            return $transaction->customerId == $customer->customerId;
        });
    }
}
