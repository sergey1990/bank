<?php

namespace Api\Package\Bank\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class TransactionFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'numeric|between:0,99999999.99',
            'date' => 'date_format:Y-m-d',
            'customerId' => 'exists:customers,customerId',
            'offset' => 'integer|min:0',
            'limit' => 'integer|min:0'
        ];
    }

}
