<?php

namespace Api\Package\Bank\Controllers;

use Api\Package\Bank\Repositories\CustomerRepository;
use Api\Package\Bank\Requests\CustomerRequest;
use Api\Package\Bank\Resources\CustomerResource;
use App\Http\Responses\JsonErrorResponse;
use Api\Package\Bank\Models\Customer;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{
    protected $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CustomerRequest $request
     * @return JsonErrorResponse|static
     */
    public function store(CustomerRequest $request)
    {
        $customer = $this
            ->repository
            ->create($request->only(['name', 'cnp']));

        if (!$customer) {
            return new JsonErrorResponse('Some issues on server.', 500);
        }

        return CustomerResource::make($customer);
    }
}