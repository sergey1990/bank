<?php

namespace Api\Package\Bank\Controllers;

use Api\Package\Bank\Criterias\TransactionCriteria;
use Api\Package\Bank\Criterias\TransactionForSpecificCustomerCriteria;
use Api\Package\Bank\Models\Transaction;
use Api\Package\Bank\Repositories\TransactionRepository;
use Api\Package\Bank\Requests\TransactionFilterRequest;
use Api\Package\Bank\Requests\TransactionRequest;
use Api\Package\Bank\Resources\TransactionResource;
use App\Http\Responses\JsonErrorResponse;
use Api\Package\Bank\Models\Customer;
use Illuminate\Routing\Controller;
use Gate;

class TransactionController extends Controller
{

    protected $repository;

    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TransactionRequest $request
     * @param Customer $customer
     * @return static
     */
    public function store(TransactionRequest $request, Customer $customer)
    {
        $transaction = $this
            ->repository
            ->createAndApplyToCustomer($request->only(['amount']), $customer);

        return TransactionResource::make($transaction);
    }

    /**
     * @param Customer $customer
     * @param Transaction $transaction
     * @return JsonErrorResponse|static
     */
    public function getOne(Customer $customer, Transaction $transaction)
    {
        if (Gate::denies('access-to-transaction', [$customer, $transaction])) {
            return new JsonErrorResponse('Not found', 404);
        }

        return TransactionResource::make($transaction);
    }

    /**
     * @param Customer $customer
     * @param TransactionForSpecificCustomerCriteria $criteria
     * @param TransactionFilterRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function get(
        Customer $customer,
        TransactionForSpecificCustomerCriteria $criteria,
        TransactionFilterRequest $request
    )
    {
        $this->repository->pushCriteria($criteria);

        $transactions = $this->repository
            ->byCustomer($customer)
            ->paginateWithOffset(
                $request->get('limit'),
                $request->get('offset')
            );

        return TransactionResource::collection($transactions);
    }

    /**
     * @param TransactionCriteria $criteria
     * @param TransactionFilterRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getAll(
        TransactionCriteria $criteria,
        TransactionFilterRequest $request
    )
    {
        $this->repository->pushCriteria($criteria);

        $transactions = $this->repository
            ->paginateWithOffset(
                $request->get('limit'),
                $request->get('offset')
            );

        return TransactionResource::collection($transactions);
    }

    /**
     * @param TransactionRequest $request
     * @param Transaction $transaction
     * @return static
     */
    public function update(TransactionRequest $request, Transaction $transaction)
    {
        $this->repository->update($request->only(['amount']), $transaction->transactionId);

        return TransactionResource::make($transaction);
    }

    /**
     * @param Transaction $transaction
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Transaction $transaction)
    {
        $this->repository->delete($transaction->transactionId);

        return response()->json([], 204);
    }
}