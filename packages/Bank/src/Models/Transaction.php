<?php

namespace Api\Package\Bank\Models;

use Digitalp\Censors\Models\Censor;
use Digitalp\Clients\Models\Client;
use Digitalp\Materials\Models\PressRelease;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Builder;

class Transaction extends Eloquent
{
    protected $primaryKey = 'transactionId';

    protected $fillable = [
        'customerId',
        'amount',
    ];

    protected $table = 'transactions';

    public function getRouteKeyName()
    {
        return 'transactionId';
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    protected $casts = [
        'amount' => 'float(10,2)',
    ];
}
