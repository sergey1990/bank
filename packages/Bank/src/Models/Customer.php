<?php

namespace Api\Package\Bank\Models;

use Digitalp\Censors\Models\Censor;
use Digitalp\Clients\Models\Client;
use Digitalp\Materials\Models\PressRelease;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Builder;

class Customer extends Eloquent
{
    protected $fillable = [
        'customerId',
        'name',
        'cnp',
    ];

    protected $table = 'customers';

    public function getRouteKeyName()
    {
        return 'customerId';
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'customerId', 'customerId');
    }
}
