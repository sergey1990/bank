<?php

namespace Api\Package\Bank\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class TransactionSum extends Eloquent
{
    public $timestamps = false;

    protected $fillable = [
        'date',
        'amount',
    ];

    protected $table = 'sum_transactions';
}
