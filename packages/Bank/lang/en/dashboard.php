<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Billing Dashboard Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'actions'       => 'Actions',
    'amount'        => 'Amount, JPY',
    'billing'       => 'Billing',
    'censor'        => 'Censor',
    'company_name'  => 'Company name',
    'date_and_time' => 'Date and time of occurrence',
    'export'        => 'Export',
    'item'          => 'Item',
    'list'          => 'List',
    'month'         => 'Month',
    'total'         => 'Total',
];
