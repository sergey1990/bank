import Vue from 'vue';
import VueRouter from 'vue-router';
import auth from './middleware/auth';

Vue.use(VueRouter);

import Home from './components/Home';
import Dashboard from './components/Dashboard';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            beforeEnter: auth
        }
    ]
});

const app = new Vue({
    el: '#app',
    router,
});