<?php

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;
use InvalidArgumentException;
use Illuminate\Support\Traits\Macroable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;
use Symfony\Component\HttpFoundation\JsonResponse as BaseJsonResponse;


class JsonErrorResponse extends JsonResponse
{
    public function __construct($data = null, $status = 200, $headers = [], $options = 0)
    {
        $dataWrapper = [];

        if(is_array($data)) {
            $dataWrapper['errors'] = $data;
        } else {
            $dataWrapper['error'] = $data;
        }

        parent::__construct($dataWrapper, $status, $headers);
    }
}