## Api docs

https://app.swaggerhub.com/apis-docs/sergey1990/bank-test-api/1#/default

## How to Install

1) I used laradock for app development, so firstly you have to clone laradock project into your project's root (of course you can choose another way for installation)

```
$ git clone https://github.com/Laradock/laradock.git
```

2) Copy .env.laradock to projectRoot/laradock/.env

3) Copy .env.sample to projectRoot/.env

4) run docker-compose

```
$ cd laradock
$ docker-compose up -d workspace nginx php-fpm mysql
$ docker-compose exec workspace php composer.phar install
$ docker-compose exec workspace php artisan migrate
$ docker-compose exec workspace php artisan db:seed
```

The App should be available by: http://127.0.0.1:8079

## Crontab file
 projectRoot/crontab.txt